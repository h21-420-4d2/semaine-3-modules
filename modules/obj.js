class TextList {
  constructor() {
    this.list = [];
  }

  add(x) {
    this.list.push(x);
  }

  length() {
    return this.list.length;
  }

  strRep() {
    return this.list.join(' ');
  }
}

const instance = new TextList();

module.exports = instance;

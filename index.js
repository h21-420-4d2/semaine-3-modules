const f1 = require('./modules/f1');
const f2 = require('./modules/f2');
const f3 = require('./modules/f3');

let pass = 1;

function ronde(txt) {
  f1(txt);
  const x = f2();
  const y = f3(x);
  console.log({
    pass,
    txt,
    x,
    y,
  });
  pass += 1;
}

ronde('Allo!');
ronde('Comment');
ronde('ça va?');
